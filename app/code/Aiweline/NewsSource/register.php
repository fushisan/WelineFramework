<?php

/*
 * 本文件由 秋枫雁飞 编写，所有解释权归Aiweline所有。
 * 邮箱：aiweline@qq.com
 * 网址：aiweline.com
 * 论坛：https://bbs.aiweline.com
 */

use Weline\Framework\Register\Register;

Register::register(
    Register::MODULE,
    __DIR__,
    '1.1.0',
    '<p>模组名：NewsSource</p>
<p>作者：秋枫雁飞（Aiweline）</p>
<p>签名：人生总要做点有意义的事儿，非是一定要成功，但是一定是在做。</p>
<a href="http://bbs.aiweline.com"></a>'
);
