<?php

/*
 * 本文件由 秋枫雁飞 编写，所有解释权归Aiweline所有。
 * 邮箱：aiweline@qq.com
 * 网址：aiweline.com
 * 论坛：https://bbs.aiweline.com
 */

namespace Weline\Framework\Controller\Data;

interface DataInterface
{
    const dir = 'Controller';

    const type_pc_FRONTEND = 'FrontendController';

    const type_pc_BACKEND = 'BackendController';

    const type_api_REST_FRONTEND = 'FrontendRestController';

    const type_api_BACKEND = 'BackendRestController';
}
