<?php

/*
 * 本文件由 秋枫雁飞 编写，所有解释权归Aiweline所有。
 * 邮箱：aiweline@qq.com
 * 网址：aiweline.com
 * 论坛：https://bbs.aiweline.com
 */

namespace Weline\Framework\App\Controller;

use Weline\Framework\Controller\AbstractRestController;

class BackendRestController extends AbstractRestController
{
    /**
     * @DESC         |登录
     *
     * 参数区：
     */
    public function login()
    {
    }
}
